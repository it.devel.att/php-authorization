<?php
  session_start();

  if (isset($_POST['email'])) { $email = $_POST['email']; if ($email == ''){ unset($email);}}
  if (isset($_POST['name'])) { $name = $_POST['name']; if ($name == ''){ unset($name);}}
  if (isset($_POST['surname'])) { $surname = $_POST['surname']; if ($surname == ''){ unset($surname);}}
  if (isset($_POST['password'])) { $password = $_POST['password']; if ($password == ''){ unset($password);}}
  if (isset($_POST['birthday'])) { $birthday = $_POST['birthday']; if ($birthday == ''){ unset($birthday);}}
  $errors_field = [];

  if (empty($email)) { $errors_field['email'] = "Email не может быть пустым";}
  if (empty($name)) { $errors_field['name'] = "Имя не может быть пустым";}
  if (empty($surname)) { $errors_field['surname'] = "Фамилия не может быть пустым";}
  if (empty($password)) { $errors_field['password'] = "Пароль не может быть пустым";}
  if (empty($birthday)) { $errors_field['birthday'] = "Пожалуйста заполните дату рождения";}
  
  $uploaddir = "uploads/";
  $uploadfile = $uploaddir . basename($_FILES['avatar']['name']);

  if (!empty($_FILES['avatar']['name'])){
    while(file_exists($uploadfile))
    {
      $uploadfile = $uploaddir . substr(md5(mt_rand()), 0, 7) . basename($_FILES['avatar']['name']);
    }

    if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile))
    {
      $errors_field['avatar'] = "Проблема при загрузке аватара";
      unset($uploadfile);
    }
  }

  if (!empty($errors_field)){
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    $flash_message = ["class" => "alert-danger", "message" => "Ошибка при регистрации!"];
    $_SESSION['flash'] = $flash_message;
    $_SESSION['field_errors'] = $errors_field;
    exit();
  }

  if (strlen($email) < 4) { $errors_field['email'] = "Длинна email не может быть меньше 4";}
  if (strlen($name) < 2) { $errors_field['name'] = "Длинна имени не может быть меньше 2";}
  if (strlen($surname) < 2) { $errors_field['surname'] = "Длинна фамилии не может быть меньше 2";}
  if (strlen($password) < 6) { $errors_field['password'] = "Пароль не должен быть короче 6 символов";}

  if (!empty($errors_field)){
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    $flash_message = ["class" => "alert-danger", "message" => "Ошибка при регистрации!"];
    $_SESSION['flash'] = $flash_message;
    $_SESSION['field_errors'] = $errors_field;
    exit();
  }

  $birthday = strtotime($birthday); 
  $birthday =  date('Y-m-d', $date);
  $email = clearString($email);
  $name = clearString($name);
  $surname = clearString($surname);
  $password = clearString($password);
  $password = password_hash($password, PASSWORD_DEFAULT);
  
  include ("db.php");

  $check_email_sql = sprintf("SELECT id, password FROM users WHERE email = '%s'", mysqli_real_escape_string($conn, $email));;
  $result = $conn->query($check_email_sql);

  if ($result->num_rows > 0)
  {
    $errors_field['email'] = "Такой email уже занят!";
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    $flash_message = ["class" => "alert-danger", "message" => "Ошибка при регистрации!"];
    $_SESSION['flash'] = $flash_message;
    $_SESSION['field_errors'] = $errors_field;
    exit();
  }

  $avatar = mysqli_real_escape_string($conn, $uploadfile);
  $email = mysqli_real_escape_string($conn, $email);
  $name = mysqli_real_escape_string($conn, $name);
  $surname = mysqli_real_escape_string($conn, $surname);
  $password = mysqli_real_escape_string($conn, $password);
  $birthday = mysqli_real_escape_string($conn, $birthday);

  $sql = "INSERT INTO users (email, name, surname, password, avatar, birthday) VALUES ('$email', '$name', '$surname', '$password', '$avatar', '$birthday')";
  if ($conn->query($sql) === TRUE)
  {
    header("Location: /");
    $flash_message = ["class" => "alert-success", "message" => "Успешная регистрация!"];
    $_SESSION['flash'] = $flash_message;
    exit();
  }
  else
  {
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    $flash_message = ["class" => "alert-danger", "message" => "Ошибка при регистрации!"];
    $_SESSION['flash'] = $flash_message;
    exit();
  }

  function clearString($string) {
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    return $string;
  }
?>