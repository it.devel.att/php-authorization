<?php 
  session_start();

  if(empty($_SESSION['user']))
  {
    header("Location: /login.php");
    exit();
  }
  else
  {
    header("Location: /profile.php");
    exit();
  }
?>