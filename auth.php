<?php
  session_start();

  if (isset($_POST['email'])) { $email = $_POST['email']; if ($email == ''){ unset($email);}}
  if (isset($_POST['password'])) { $password = $_POST['password']; if ($password == ''){ unset($password);}}

  $errors_field = [];

  if (empty($email)) { $errors_field['email'] = "Введите email";}
  if (empty($password)) { $errors_field['password'] = "Введите пароль";}

  if (!empty($errors_field)){
    header('Location: /');
    $flash_message = ["class" => "alert-danger", "message" => "Ошибка при аутентификации!"];
    $_SESSION['flash'] = $flash_message;
    $_SESSION['field_errors'] = $errors_field;
    exit();
  }

  include ("db.php");
  
  $get_hash_password = sprintf("SELECT id, password FROM users WHERE email = '%s'", mysqli_real_escape_string($conn, $email));
  $result = $conn->query($get_hash_password);
  
  if ($result->num_rows == 0)
  {
    header('Location: /');
    $flash_message = ["class" => "alert-danger", "message" => "Неверный email или пароль!"];
    $_SESSION['flash'] = $flash_message;
    exit();
  }
  else
  {
    $row = $result -> fetch_assoc();
    $hash_password = $row['password'];
    if (password_verify($password, $hash_password))
    {
      $_SESSION['user'] = $row['id'];
      header('Location: /');
      exit();
    }
    else
    {
      header('Location: /');
      $flash_message = ["class" => "alert-danger", "message" => "Неверный email или пароль!"];
      $_SESSION['flash'] = $flash_message;
      exit();
    }
  }
?>