<?php 
  session_start(); 
  $field_email_error = $_SESSION['field_errors']['email'];
  $field_name_error = $_SESSION['field_errors']['name'];
  $field_surname_error = $_SESSION['field_errors']['surname'];
  $field_password_error = $_SESSION['field_errors']['password'];
  $field_avatar_error = $_SESSION['field_errors']['avatar'];
  $field_birthday_error = $_SESSION['field_errors']['birthday'];
  
  include "templates/registration.html";
  
  unset($_SESSION['field_errors']);
?>
