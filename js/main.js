$(document).ready(function(){
  $('#signInForm').hide().fadeIn(1000);
  $('#registrationForm').hide().fadeIn(1000);

  if ($("#flashMessage").length !== 0){
    $("#flashMessage").fadeOut(3000);
  }

  $('#avatarUploadInput').on('change', function(){
    $('#placeForPreviewUploadFile').empty();
    const reader = new FileReader();

    reader.onload = function (e){
      $('#placeForPreviewUploadFile').append("<img src=" + e.target.result + " alt=\"avatar preview\" class=\"avatar-profile\" >")
    };
    reader.readAsDataURL(this.files[0])
  })
});
