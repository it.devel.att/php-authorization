<?php if(isset($_SESSION['flash'])): ?>
  <div id="flashMessage" class="alert <?php echo $_SESSION['flash']['class'] ?>"  >
    <?php echo $_SESSION['flash']['message'] ?>
  </div>
  <?php unset($_SESSION['flash']) ?>
<?php endif;?>