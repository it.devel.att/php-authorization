<?php session_start();
  $user_id = $_SESSION['user'];

  include ("db.php");

  $get_user = sprintf("SELECT id, name, surname, email, avatar FROM users WHERE id = '%s'", mysqli_real_escape_string($conn, $user_id));
  $result = $conn->query($get_user);

  if ($result->num_rows == 0)
  {
    unset($_SESSION['user']);
    header('Location: /');
    $flash_message = ["class" => "alert-danger", "message" => "Пользователь больше не существует!"];
    $_SESSION['flash'] = $flash_message;
    exit();
  }
  else
  {
    $row = $result -> fetch_assoc();
    $user['email'] = $row['email'];
    $user['name'] = $row['name'];
    $user['surname'] = $row['surname'];
    $user['avatar'] = $row['avatar'];

    include "templates/profile.html";
  }
?>
